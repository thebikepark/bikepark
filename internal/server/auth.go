package server

import (
	"context"
	"errors"

	"gitlab.com/thebikepark/bikepark/internal/app"
	"gitlab.com/thebikepark/bikepark/internal/business/users"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/postgres"
	"gitlab.com/thebikepark/bikepark/pkg/auth"
	"gitlab.com/thebikepark/bikepark/pkg/bikepark"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

var (
	_                bikepark_data_v1.BikeParkAuthServer = &AuthService{}
	wrongCredentials                                     = &errdetails.BadRequest{
		FieldViolations: []*errdetails.BadRequest_FieldViolation{
			{
				Field:       "credentials",
				Description: "Wrong username/password combination",
			},
		},
	}

	missingPassword = &errdetails.BadRequest{
		FieldViolations: []*errdetails.BadRequest_FieldViolation{
			{
				Field:       "password",
				Description: "Password required",
			},
		},
	}

	missingUsername = &errdetails.BadRequest{
		FieldViolations: []*errdetails.BadRequest_FieldViolation{
			{
				Field:       "username",
				Description: "Username required",
			},
		},
	}
)

// AuthService bike park authentiction service
type AuthService struct {
	*bikepark_data_v1.UnimplementedBikeParkAuthServer
	db          postgres.AuthDatabase
	auth        auth.Authenticator
	log         *zap.Logger
	userManager *postgres.UsersManager
}

// NewAuthService returns new bike park authentication service
func NewAuthService(log *zap.Logger, db postgres.AuthDatabase, dbNew *postgres.PsqlDatabase, auth auth.Authenticator) *AuthService {
	return &AuthService{
		db:          db,
		log:         log,
		auth:        auth,
		userManager: postgres.NewUsersManager(dbNew),
	}
}

// Login gets users from database and generate jwt token
func (s *AuthService) Login(ctx context.Context, in *bikepark_data_v1.LoginRequest) (*bikepark_data_v1.LoginResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.BikeParkAuthServer.Login"))

	user, err := s.db.GetUserByUsername(ctx, in.GetUsername())
	if err != nil {
		log.Error("failed to get user", zap.Error(err))
		errS, err := status.New(codes.Unauthenticated, "BikePark account doesn't exist").WithDetails(wrongCredentials)
		if err != nil {
			log.Error("failed to build error with details", zap.Error(err))
			return nil, bpErrors.InternalServerError().Err()
		}
		return nil, errS.Err()
	}

	same := app.ComparePasswords(in.GetPassword(), user.Password)
	if !same {
		log.Error("invalid password", zap.Error(errors.New("bad password")))
		errS, err := status.New(codes.Unauthenticated, "Invalid password").WithDetails(wrongCredentials)
		if err != nil {
			log.Error("failed to build error with details", zap.Error(err))
			return nil, bpErrors.InternalServerError().Err()
		}
		return nil, errS.Err()
	}

	token, err := s.auth.GenerateToken(auth.UserClaims{Username: user.Username, UserID: user.ID})
	if err != nil {
		log.Error("failed to generate jwt token", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished login request")

	return &bikepark_data_v1.LoginResponse{Token: token}, nil
}

func (s *AuthService) Register(ctx context.Context, in *bikepark_data_v1.AccountRegistrationRequest) (*emptypb.Empty, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.BikeParkAuthServer.Register"))

	if in.GetUsername() == "" {
		errS, err := status.New(codes.InvalidArgument, "Username required").WithDetails(missingUsername)
		if err != nil {
			log.Error("failed to build error with details", zap.Error(err))
			return nil, bpErrors.InternalServerError().Err()
		}
		log.Error("missing username", zap.Error(errS.Err()))
		return nil, errS.Err()
	}

	if in.GetPassword() == "" {
		errS, err := status.New(codes.InvalidArgument, "Password required").WithDetails(missingPassword)
		if err != nil {
			log.Error("failed to build error with details", zap.Error(err))
			return nil, bpErrors.InternalServerError().Err()
		}
		log.Error("missing password", zap.Error(errS.Err()))
		return nil, errS.Err()
	}

	u := &bikepark_data_v1.User{
		Username: in.GetUsername(),
		Password: bikepark.String(in.GetPassword()),
		FullName: in.GetFullName(),
	}
	_, err := users.Create(ctx, u, s.userManager)
	if err != nil {
		log.Error("failed to create new account", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateUserErrMsg).Err()
	}

	log.Debug("successfully finished register request")

	return &emptypb.Empty{}, nil
}
