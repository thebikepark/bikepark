package server

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/thebikepark/bikepark/internal/app"
	"gitlab.com/thebikepark/bikepark/internal/business/reviews"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (s *Service) CreateReview(ctx context.Context, in *v1.CreateReviewRequest) (*v1.Review, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.CreateReview"))

	if err := validateReview(in.GetReview()); err != nil {
		log.Error("invalid review", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, "Validation error").Err()
	}

	log = s.log.With(
		zap.String("placeID", in.GetReview().GetPlaceId()),
	)

	userUUID, err := app.UserIDFromCtx(ctx)
	if err != nil {
		log.Error("failed to extract ID from ctx", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateReviewErrMsg).Err()
	}

	log = s.log.With(
		zap.String("userID", userUUID.String()),
	)

	in.Review.UserId = userUUID.String()

	createdReview, err := reviews.Create(ctx, in.GetReview(), s.reviewsManager)
	if err != nil {
		log.Error("failed to create review", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateReviewErrMsg).Err()
	}

	review, err := models.ReviewToAPIReview(createdReview)
	if err != nil {
		log.Error("failed to parse review to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished create review")

	return review, nil
}

func (s *Service) EditReview(ctx context.Context, in *v1.EditReviewRequest) (*v1.Review, error) {
	log := s.log.With(
		zap.String("handler", "bikepark/server.Service.EditReview"),
	)

	if err := validateReview(in.GetReview()); err != nil {
		log.Error("invalid review", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, "Validation error").Err()
	}

	log = s.log.With(
		zap.String("placeID", in.GetReview().GetPlaceId()),
	)

	userUUID, err := app.UserIDFromCtx(ctx)
	if err != nil {
		log.Error("failed to extract ID from ctx", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateReviewErrMsg).Err()
	}

	log = s.log.With(
		zap.String("userID", userUUID.String()),
	)

	updatedReview, err := reviews.Create(ctx, in.GetReview(), s.reviewsManager)
	if err != nil {
		log.Error("failed to update review", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.EditReviewErrMsg).Err()
	}

	review, err := models.ReviewToAPIReview(updatedReview)
	if err != nil {
		log.Error("failed to parse review to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished edit review")

	return review, nil
}

func (s *Service) ListReviews(ctx context.Context, in *v1.ListReviewsRequest) (*v1.ListReviewsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.ListReviews"))

	listedReviews, err := reviews.List(ctx, in.Filter, s.reviewsManager)
	if err != nil {
		log.Error("failed to fetch reviews", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetReviewErrMsg).Err()
	}

	apiReviews := make([]*v1.Review, 0, len(listedReviews))
	for _, r := range listedReviews {
		apiReview, err := models.ReviewToAPIReview(r)
		if err != nil {
			log.Error("failed to parse review to API", zap.Error(err))
			return nil, bpErrors.InternalServerError().Err()
		}
		apiReviews = append(apiReviews, apiReview)
	}

	log.Debug("successfully finished list reviews")

	return &v1.ListReviewsResponse{
		Reviews: apiReviews,
	}, nil
}

func (s *Service) MyReviews(ctx context.Context, in *v1.MyReviewsRequest) (*v1.ListReviewsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.ListReviews"))

	userUUID, err := app.UserIDFromCtx(ctx)
	if err != nil {
		log.Error("failed to extract ID from ctx", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateReviewErrMsg).Err()
	}

	listedReviews, err := reviews.List(ctx, &v1.ReviewsFilter{
		UserId: userUUID.String(),
	}, s.reviewsManager)
	if err != nil {
		log.Error("failed to fetch reviews", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetReviewErrMsg).Err()
	}

	apiReviews := make([]*v1.Review, 0, len(listedReviews))
	for _, r := range listedReviews {
		apiReview, err := models.ReviewToAPIReview(r)
		if err != nil {
			log.Error("failed to parse review to API", zap.Error(err))
			return nil, bpErrors.InternalServerError().Err()
		}
		apiReviews = append(apiReviews, apiReview)
	}

	log.Debug("successfully finished list reviews")

	return &v1.ListReviewsResponse{
		Reviews: apiReviews,
	}, nil
}

func (s *Service) DeleteReview(ctx context.Context, in *v1.DeleteReviewRequest) (*emptypb.Empty, error) {
	log := s.log.With(
		zap.String("handler", "bikepark/server.Service.DeleteReview"),
		zap.String("review", in.GetName()),
	)

	userUUID, err := app.UserIDFromCtx(ctx)
	if err != nil {
		log.Error("failed to extract ID from ctx", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteReviewErrMsg).Err()
	}

	log = s.log.With(
		zap.String("userID", userUUID.String()),
	)

	reviewID, err := app.ExtractIDFromResourceName(in.GetName(), 2)
	if err != nil {
		log.Error("failed to extract from resource name", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteReviewErrMsg).Err()
	}

	if err := reviews.Delete(ctx, reviewID, userUUID, s.reviewsManager); err != nil {
		log.Error("failed to delete review", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteReviewErrMsg).Err()
	}

	log.Debug("successfully finished delete review")

	return &emptypb.Empty{}, nil
}

func validateReview(r *v1.Review) error {
	if r == nil {
		return errors.New("missing review")
	}

	if r.GetPlaceId() == "" {
		err := bpErrors.NewValidationErr(
			errors.New("place id required"),
			bpErrors.Details("place", "Place is required"),
		)
		return err
	}

	if r.GetRating() <= 0 {
		err := bpErrors.NewValidationErr(
			fmt.Errorf("invalid rating value: %d", r.GetRating()),
			bpErrors.Details("place", "Place is required"),
		)
		return err
	}

	return nil
}
