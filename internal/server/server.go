package server

import (
	"context"

	"gitlab.com/thebikepark/bikepark/internal/postgres"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
)

var _ bikepark_data_v1.BikeParkServer = &Service{}

// Service represnets gRPC server
type Service struct {
	*bikepark_data_v1.UnimplementedBikeParkServer
	bpm            *postgres.BikeParkManager
	btm            *postgres.BikeTheftManager
	um             *postgres.UsersManager
	reviewsManager *postgres.ReviewsManager
	log            *zap.Logger
}

// New returns new bike park service
func New(ctx context.Context, log *zap.Logger, dbNew *postgres.PsqlDatabase) (*Service, error) {
	um := postgres.NewUsersManager(dbNew)
	bpm := postgres.NewBikeParkManager(dbNew, um)
	btm := postgres.NewBikeTheftManager(dbNew, um)
	rm := postgres.NewReviewsManager(dbNew)

	return &Service{
		bpm:            bpm,
		um:             um,
		btm:            btm,
		reviewsManager: rm,
		log:            log,
	}, nil
}
