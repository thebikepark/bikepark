package server

import (
	"context"

	"gitlab.com/thebikepark/bikepark/internal/app"
	"gitlab.com/thebikepark/bikepark/internal/business/users"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
	"google.golang.org/protobuf/types/known/emptypb"
)

// EditUser creates new user
func (s *Service) EditUser(ctx context.Context, in *bikepark_data_v1.EditUserRequest) (*bikepark_data_v1.User, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.EditUser"))

	userUUID, err := app.UserIDFromCtx(ctx)
	if err != nil {
		log.Error("failed to extract ID from ctx", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.UpdateUserLocationErrMsg).Err()
	}

	u := models.APIUserToData(in.GetUser())
	u.ID = userUUID

	updatedUser, err := users.Update(ctx, u, s.um)
	if err != nil {
		log.Error("failed to edit user", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.EditUserErrMsg).Err()
	}

	log.Debug("successfully finished delete edit user request")

	return models.DataUserToAPI(updatedUser), nil
}

// GetUser returns user by name
func (s *Service) GetUser(ctx context.Context, in *bikepark_data_v1.GetUserRequest) (*bikepark_data_v1.User, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.GetUser"))

	u, err := users.Get(ctx, in.GetName(), s.um)
	if err != nil {
		log.Error("failed to get user", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetUserErrMsg).Err()
	}

	log.Debug("successfully finished get user request")

	return models.DataUserToAPI(u), nil
}

// ListUsers returns users
func (s *Service) ListUsers(ctx context.Context, in *bikepark_data_v1.ListUsersRequest) (*bikepark_data_v1.ListUsersResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.ListUsers"))

	dbUsers, err := users.List(ctx, s.um)
	if err != nil {
		log.Error("failed to get users", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.NoIDErrMsg).Err()
	}

	log.Debug("successfully finished list users request")

	return &bikepark_data_v1.ListUsersResponse{
		Users: models.DataUsersToAPI(dbUsers),
	}, nil
}

// DeleteUser set user is active status to false
func (s *Service) DeleteUser(ctx context.Context, in *bikepark_data_v1.DeleteUserRequest) (*emptypb.Empty, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.DeleteUser"))

	ID, err := app.ExtractIDFromResourceName(in.GetName(), 2)
	if err != nil {
		log.Error("failed to extract ID from resource", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.NoIDErrMsg).Err()
	}
	if err := users.Delete(ctx, ID, s.um); err != nil {
		log.Error("failed to delete user", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteUserErrMsg).Err()
	}

	log.Debug("successfully finished delete user request")

	return &emptypb.Empty{}, nil
}

// EditUserLocation edits user location
func (s *Service) EditUserLocation(ctx context.Context, in *bikepark_data_v1.UserLocationRequest) (*bikepark_data_v1.UserLocation, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.EditUserLocation"))

	userUUID, err := app.UserIDFromCtx(ctx)
	if err != nil {
		log.Error("failed to extract ID from ctx", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.UpdateUserLocationErrMsg).Err()
	}

	log = log.With(zap.String("userUUID", userUUID.String()))

	updatedUserLocation, err := users.UpdatLocation(ctx, userUUID, in.GetUserLocation(), s.um)
	if err != nil {
		log.Error("failed to update user location", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.UpdateUserLocationErrMsg).Err()
	}

	log.Debug("successfully finished edit user location request")

	return models.DataUserLocationToAPI(updatedUserLocation), nil
}
