package server

import (
	"context"

	"gitlab.com/thebikepark/bikepark/internal/app"
	"gitlab.com/thebikepark/bikepark/internal/business/bikethefts"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

// CreateBikeTheft creates new bike theft
func (s *Service) CreateBikeTheft(ctx context.Context, in *bikepark_data_v1.CreateBikeTheftRequest) (*bikepark_data_v1.BikeTheft, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.CreateBikeTheft"))

	btDB := models.APITheftToData(in.GetBikeTheft())

	bt, err := bikethefts.Create(ctx, btDB, s.btm)
	if err != nil {
		log.Error("failed to create a new bike theft", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateBikeTheftErrMsg).Err()
	}

	log.Debug("successfully finished create bike theft request")

	return models.DataTheftToAPI(bt), nil
}

// EditBikeTheft edit the bike theft
func (s *Service) EditBikeTheft(ctx context.Context, in *bikepark_data_v1.EditBikeTheftRequest) (*bikepark_data_v1.BikeTheft, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.EditBikeTheft"))

	btDB := models.APITheftToData(in.GetBikeTheft())

	newBt, err := bikethefts.Update(ctx, btDB, s.btm)
	if err != nil {
		log.Error("failed to edit bike theft", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.EditBikeTheftErrMsg).Err()
	}

	log.Debug("successfully finished edit bike theft request")

	return models.DataTheftToAPI(newBt), nil
}

// ListBikeThefts returns list of bike thefts
func (s *Service) ListBikeThefts(ctx context.Context, in *bikepark_data_v1.ListBikeTheftsRequest) (*bikepark_data_v1.ListBikeTheftsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.ListBikeThefts"))

	btsDB, err := bikethefts.List(ctx, s.btm)
	if err != nil {
		log.Error("failed to fetch bike thefts", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetBikeTheftErrMsg).Err()
	}

	log.Debug("successfully finished list bike theft request")

	return &bikepark_data_v1.ListBikeTheftsResponse{
		BikeThefts: models.DataTheftsToAPI(btsDB),
	}, nil
}

// DeleteBikeTheft deletes bike theft
func (s *Service) DeleteBikeTheft(ctx context.Context, in *bikepark_data_v1.DeleteBikeTheftRequest) (*emptypb.Empty, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.DeleteBikeTheft"))

	UUID, err := app.ExtractIDFromResourceName(in.Name, 2)
	if err != nil {
		log.Error("failed to extract ID from resource", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteBikeTheftErrMsg).Err()
	}

	if err := bikethefts.Delete(ctx, UUID, s.btm); err != nil {
		log.Error("failed to delete bike theft", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteBikeTheftErrMsg).Err()
	}

	log.Debug("successfully finished delete bike theft request")

	return &emptypb.Empty{}, nil
}

// MyBikeThefts returns list of bike thefts creted by logged in user. It will filer bike thefts by user id.
func (s *Service) MyBikeThefts(ctx context.Context, in *emptypb.Empty) (*bikepark_data_v1.ListBikeTheftsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.MyBikeThefts"))

	btsDB, err := bikethefts.My(ctx, s.btm)
	if err != nil {
		log.Error("failed to fetch bike thefts", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetBikeTheftErrMsg).Err()
	}

	log.Debug("successfully finished my bike theft request")

	return &bikepark_data_v1.ListBikeTheftsResponse{
		BikeThefts: models.DataTheftsToAPI(btsDB),
	}, nil
}

// NearestBikeThefts returns list of nearest bike thefts based on latitude and longitude.
func (s *Service) NearestBikeThefts(ctx context.Context, in *bikepark_data_v1.NearestBikeTheftsRequest) (*bikepark_data_v1.NearestBikeTheftsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.NearestBikeThefts"))

	limit := int32(50)
	if in.Limit > 0 {
		limit = in.Limit
	}
	if in.Filter == nil {
		log.Error("missing filter")
		return nil, status.New(codes.InvalidArgument, "Filter is required").Err()
	}
	if in.Filter.Distance <= 0 {
		in.Filter.Distance = 2
	}

	if err := validateNearestTheftFilter(in.Filter); err != nil {
		log.Error("invalid nearest theft filter", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, "Invalid Arguments").Err()
	}

	thefts, err := bikethefts.Nearest(ctx, s.btm, in.GetFilter(), limit)
	if err != nil {
		log.Error("fetch nearest bike thefts", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetBikeTheftErrMsg).Err()
	}

	log.Debug("successfully finished nearest bike theft request")

	return &bikepark_data_v1.NearestBikeTheftsResponse{
		BikeThefts: models.DataTheftsToAPI(thefts),
	}, nil
}
