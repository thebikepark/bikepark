package errors

import (
	"errors"

	"gitlab.com/thebikepark/bikepark/internal/postgres"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
)

const (
	errInternal = "An error occurred"
	// errNotAllowedToPerformAction = "You are not allowed to perform this action"
	NoIDErrMsg = "There is no ID in URL. ID should be integer."

	// SometingWentWrong used mostly for interal server error
	SometingWentWrong = "Something went wrong."
)

// NewAPIStatusErr returs new status error with details
func NewAPIStatusErr(e error, msg string, d ...proto.Message) *status.Status {
	if errors.Is(e, &postgres.NotFoundErr{}) {
		return status.New(codes.NotFound, msg)
	}

	if errors.Is(e, &postgres.Error{}) {
		return status.New(codes.Internal, msg)
	}

	var uniqueErr *postgres.UniqueViolationErr
	if errors.As(e, &uniqueErr) {
		s, err := status.New(codes.AlreadyExists, msg).WithDetails(&errdetails.BadRequest{
			FieldViolations: []*errdetails.BadRequest_FieldViolation{
				{
					Field:       uniqueErr.Key,
					Description: uniqueErr.Msg,
				},
			},
		})
		if err != nil {
			return status.New(codes.Internal, errInternal)
		}
		return s
	}

	var validationErr *ValidationErr
	if errors.As(e, &validationErr) {
		s, err := status.New(codes.InvalidArgument, msg).WithDetails(validationErr.BadRequestDetails)
		if err != nil {
			return status.New(codes.Internal, errInternal)
		}
		return s
	}

	errMsg := errInternal
	if msg != "" {
		errMsg = msg
	}
	return status.New(codes.Internal, errMsg)
}

// PermissionDeniedErr returns permission denied error
func PermissionDeniedErr2(msg string) *status.Status {
	return status.New(codes.PermissionDenied, msg)
}

// InternalServerError returns internal server error
func InternalServerError() *status.Status {
	return NewAPIStatusErr(nil, "")
}

// ValidationErr holds info for not found error
type ValidationErr struct {
	Err               error
	BadRequestDetails *errdetails.BadRequest
}

// NewValidationErr returns new validation error
func NewValidationErr(err error, details *errdetails.BadRequest) *ValidationErr {
	return &ValidationErr{err, details}
}

func (ve *ValidationErr) Error() string {
	return ve.Err.Error()
}

// Details returns proto message of validation details
func Details(field, description string) *errdetails.BadRequest {
	return &errdetails.BadRequest{
		FieldViolations: []*errdetails.BadRequest_FieldViolation{
			{
				Field:       field,
				Description: description,
			},
		},
	}
}

// Is match error in error chain
func (ve *ValidationErr) Is(err error) bool {
	_, ok := err.(*ValidationErr)
	return ok
}

// UniqueViolationErr holds info for unique database errors
type UniqueViolationErr struct {
	Key   string
	Msg   string
	inner error
}

func (uve *UniqueViolationErr) Error() string {
	return uve.inner.Error()
}

// NewUniqueViolationErr returns new unique violation error
func NewUniqueViolationErr(key, msg string, inner error) *UniqueViolationErr {
	return &UniqueViolationErr{
		Key:   key,
		Msg:   msg,
		inner: inner,
	}
}

// Is match error in error chain
func (uve *UniqueViolationErr) Is(err error) bool {
	_, ok := err.(*UniqueViolationErr)
	return ok
}
