package app

import (
	"context"
	"testing"

	"github.com/c2fo/testify/require"
	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/pkg/auth"
	"google.golang.org/grpc/metadata"
)

func TestRequesterFromCtx(t *testing.T) {
	ctx := context.Background()

	userID := uuid.New()

	fullMetadata := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.UsernameHeaderKey: []string{"test"},
		auth.UUIDHeaderKey:     []string{userID.String()},
	})
	noUsername := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.UsernameHeaderKey: []string{},
		auth.UUIDHeaderKey:     []string{"1"},
	})
	noUserID := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.UsernameHeaderKey: []string{"test"},
		auth.UUIDHeaderKey:     []string{},
	})

	invalidUserID := metadata.NewIncomingContext(ctx, metadata.MD{
		auth.UsernameHeaderKey: []string{"test"},
		auth.UUIDHeaderKey:     []string{"123213"},
	})

	outgoing := metadata.NewOutgoingContext(ctx, nil)

	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		args    args
		want    Requester
		wantErr bool
	}{
		{
			name: "should return username and user id",
			args: args{
				ctx: fullMetadata,
			},
			want: Requester{
				UserID:   userID,
				Username: "test",
			},
		},
		{
			name: "should return no username in context",
			args: args{
				ctx: noUsername,
			},
			wantErr: true,
		},
		{
			name: "should return no user id in context",
			args: args{
				ctx: noUserID,
			},
			wantErr: true,
		},
		{
			name: "should return if invalid user id in context",
			args: args{
				ctx: invalidUserID,
			},
			wantErr: true,
		},
		{
			name: "should return if outgoing context is passed",
			args: args{
				ctx: outgoing,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, err := RequesterFromCtx(tt.args.ctx)
			if tt.wantErr {
				require.Error(t, err)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, r)
		})
	}
}
