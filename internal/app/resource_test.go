package app

import (
	"testing"

	"github.com/c2fo/testify/require"
	"github.com/google/uuid"
)

func TestExtractIDFromResourceName(t *testing.T) {

	id := uuid.New()

	type args struct {
		s        string
		position int
	}
	tests := []struct {
		name    string
		args    args
		want    uuid.UUID
		wantErr bool
		errMsg  string
	}{
		{
			name: "it should return id from postion 1",
			args: args{
				s:        "test/" + id.String(),
				position: 2,
			},
			want: id,
		},
		{
			name: "it should return error (position is bigger than number of elems)",
			args: args{
				s:        "test/123",
				position: 3,
			},
			errMsg:  "position is bigger than resource positions",
			wantErr: true,
		},
		{
			name: "it should return error (no resource)",
			args: args{
				s:        "",
				position: 2,
			},
			errMsg:  "please provide resource",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractIDFromResourceName(tt.args.s, tt.args.position)
			if tt.wantErr {
				require.Error(t, err)
				require.Equal(t, tt.errMsg, err.Error())
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestUserResourceName(t *testing.T) {
	id := uuid.New()

	type args struct {
		uuid uuid.UUID
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "it should return user resource name",
			args: args{
				uuid: id,
			},
			want: "users/" + id.String(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := UserResourceName(tt.args.uuid)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestBikeParkingResourceName(t *testing.T) {
	id := uuid.New()

	type args struct {
		uuid uuid.UUID
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "it should return bike parking resource name",
			args: args{
				uuid: id,
			},
			want: "bikeparkings/" + id.String(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := BikeParkResourceName(tt.args.uuid)
			require.Equal(t, tt.want, got)
		})
	}
}
