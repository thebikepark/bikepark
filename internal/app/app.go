package app

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/pkg/auth"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/metadata"
)

// Requester hold request data extracted from incoming context
type Requester struct {
	Username string
	UserID   uuid.UUID
}

// ComparePasswords comapers password with hash
func ComparePasswords(password string, to string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(to), []byte(password))
	return err == nil
}

// RequesterFromCtx returns username and user id from incoming context
func RequesterFromCtx(ctx context.Context) (Requester, error) {
	r := Requester{}
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return r, errors.New("failed to get metadata from incoming context")
	}
	username := md.Get(auth.UsernameHeaderKey)
	if len(username) < 1 {
		return r, errors.New("no username in context metadata")
	}

	userID := md.Get(auth.UUIDHeaderKey)
	if len(userID) < 1 {
		return r, errors.New("no name in context medata")
	}

	userUUID, err := uuid.Parse(userID[0])
	if err != nil {
		return r, err
	}

	r.Username = username[0]
	r.UserID = userUUID
	return r, nil
}

func UserIDFromCtx(ctx context.Context) (uuid.UUID, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return uuid.Nil, errors.New("failed to get metadata from incoming context")
	}

	userID := md.Get(auth.UUIDHeaderKey)
	if len(userID) < 1 {
		return uuid.Nil, errors.New("no name in context medata")
	}

	userUUID, err := uuid.Parse(userID[0])
	if err != nil {
		return uuid.Nil, err
	}

	if userUUID == uuid.Nil {
		return uuid.Nil, errors.New("missing authorization user id")
	}

	return userUUID, nil
}
