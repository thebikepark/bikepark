package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/lib/pq"
)

const (
	// UniqueValiationKey postgres unique key
	UniqueValiationKey = "23505"
)

// DBConfig database connection info
type DBConfig struct {
	User string
	Pass string
	Host string
	Port string
	Name string
}

// PsqlDatabase implements a Postgres database
type PsqlDatabase struct {
	db *pgxpool.Pool
}

// NewPsqlConn returns new postgres database connection
func NewPsqlConn(c DBConfig) (*PsqlDatabase, error) {
	dbURL := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?pool_max_conns=3&pool_max_conn_lifetime=1h", c.Name, c.Pass, c.Host, c.Port, c.Name)
	pgxConn, err := new(context.Background(), dbURL)
	if err != nil {
		return nil, err
	}

	return &PsqlDatabase{
		db: pgxConn,
	}, nil
}

// Close will close the database connection.
func (pq *PsqlDatabase) Close() {
	pq.db.Close()
}

// Ping will attempt to actually connect to the database and validate the connection string
func (pq *PsqlDatabase) Ping() error {
	return pq.db.Ping(context.Background())
}

// IsPostgresErrCode check if error code is part of postgres error codes
func IsPostgresErrCode(code string, err error) bool {
	if err, ok := err.(*pq.Error); ok {
		return string(err.Code) == code
	}
	return false
}

// Code check if error code is part of postgres error codes
func Code(err error) string {
	if err, ok := err.(*pgconn.PgError); ok {
		return err.Code
	}
	return ""
}

// New returns new postgresql connection
// pool_max_conns: integer greater than 0
// pool_min_conns: integer 0 or greater
// pool_max_conn_lifetime: duration string
// pool_max_conn_idle_time: duration string
// pool_health_check_period: duration string
// postgres://postgres://username:password@localhost:5432/database_name/mydb?sslmode=verify-ca&pool_max_conns=10
func new(ctx context.Context, dbURL string) (*pgxpool.Pool, error) {
	conn, err := pgxpool.Connect(ctx, dbURL)
	if err != nil {
		return nil, fmt.Errorf("failed create database connection: %w", err)
	}

	if err := conn.Ping(ctx); err != nil {
		return nil, fmt.Errorf("failed to ping database: %w", err)
	}

	return conn, nil
}
