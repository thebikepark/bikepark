package postgres

import (
	"github.com/doug-martin/goqu/v9"
)

// Db implements Database interface
type Db struct {
	*goqu.Database
}
