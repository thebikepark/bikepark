package postgres

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/models"
)

type (
	BikeTheftManager struct {
		db *PsqlDatabase
		um *UsersManager
	}
)

func NewBikeTheftManager(db *PsqlDatabase, um *UsersManager) *BikeTheftManager {
	return &BikeTheftManager{db: db, um: um}
}

// Create creates new bike theft location
func (btm *BikeTheftManager) Create(ctx context.Context, bt models.BikeTheft) (models.BikeTheft, error) {
	var (
		out         = bt
		insertQuery = `
		INSERT INTO 
			bike_thefts(user_id, street, city, latitude, longitude, description)
		VALUES
			($1, $2, $3, $4, $5, $6)
		RETURNING
			id,
			created_at,
			updated_at
		`
	)

	err := btm.db.db.QueryRow(ctx, insertQuery, bt.UserID, bt.Street, bt.City, bt.Latitude, bt.Longitude, bt.Description).
		Scan(
			&out.ID,
			&out.CreatedAt,
			&out.UpdatedAt,
		)
	if err != nil {
		return out, NewError(err, "failed to insert new bike theft into database")
	}

	u, err := btm.um.Get(ctx, bt.UserID)
	if err != nil {
		return out, NewError(err, "failed to get user from database")
	}

	out.User = u

	return out, nil
}

// Update edit bike theft location
func (btm *BikeTheftManager) Update(ctx context.Context, bt models.BikeTheft) (models.BikeTheft, error) {
	var (
		out         = bt
		updateQuery = `
		UPDATE bike_thefts
		SET 
			street = $1,
			city = $2,
			latitude = $3,
			longitude = $4,
			description = $5,
			is_found = $6
		WHERE
			id = $7
		AND
			user_id = $8
		RETURNING
			created_at,
			updated_at`
	)

	err := btm.db.db.QueryRow(ctx, updateQuery, bt.Street, bt.City, bt.Latitude, bt.Longitude, bt.Description, bt.IsFound, bt.ID, bt.UserID).
		Scan(
			&out.CreatedAt,
			&out.UpdatedAt,
		)
	if err != nil {
		return out, NewError(err, "failed to update bike theft")
	}

	u, err := btm.um.Get(ctx, bt.UserID)
	if err != nil {
		return out, NewError(err, "failed to get user from database")
	}
	out.User = u

	return out, nil
}

// List returns list of bike thefts
func (btm *BikeTheftManager) List(ctx context.Context, f models.BikeTheftsFilter) ([]models.BikeTheft, error) {
	var (
		whereClause []string
		args        []interface{}
		selectQuery = `
	SELECT
		bt.id, bt.user_id, bt.street, bt.city, bt.description, bt.latitude, bt.longitude, bt.is_found, bt.created_at, bt.updated_at,
		u.id, u.username, u.full_name, u.is_active, u.created_at, u.updated_at
	FROM
		bike_thefts bt
	JOIN
		users u
		ON
			u.id = bt.user_id
	%s
	ORDER BY
		bt.created_at DESC
	`
	)

	if f.UserID != uuid.Nil {
		whereClause = append(whereClause, "bt.user_id = $1")
		args = append(args, f.UserID)
	}

	if f.IsDeleted != nil && !*f.IsDeleted {
		whereClause = append(whereClause, "bt.deleted_at IS NULL")
	} else {
		whereClause = append(whereClause, "bt.deleted_at IS NOT NULL")
	}

	conditions := strings.Join(whereClause, " AND ")
	if conditions != "" {
		conditions = "WHERE " + conditions
	}

	selectQuery = fmt.Sprintf(selectQuery, conditions)
	rows, err := btm.db.db.Query(ctx, selectQuery, args...)
	if err != nil {
		return nil, NewError(err, "failed to list bike thefts")
	}
	defer rows.Close()

	var bts []models.BikeTheft
	for rows.Next() {
		var bt models.BikeTheft
		var u models.User
		if err := rows.Scan(
			&bt.ID,
			&bt.UserID,
			&bt.Street,
			&bt.City,
			&bt.Description,
			&bt.Latitude,
			&bt.Longitude,
			&bt.IsFound,
			&bt.CreatedAt,
			&bt.UpdatedAt,
			&u.ID,
			&u.Username,
			&u.FullName,
			&u.IsActive,
			&u.CreatedAt,
			&u.UpdatedAt,
		); err != nil {
			return nil, NewError(err, "failed to scan bike theft row")
		}
		bt.User = u
		bts = append(bts, bt)
	}

	return bts, nil
}

// Delete delets bike theft
func (btm *BikeTheftManager) Delete(ctx context.Context, UUID, userID uuid.UUID) error {
	_, err := btm.db.db.Exec(ctx, "UPDATE bike_thefts SET deleted_at = NOW() WHERE id = $1 AND user_id = $2", UUID, userID)
	if err != nil {
		return NewError(err, "failed to execute delete location sql query")
	}

	return nil
}

// Nearest query nearest location based on filter
// select *
// 	from (
// 		select *,
// 		(
// 			acos(
// 				sin(radians(45.807434)) *
// 				sin(radians(latitude)) +
// 				cos(radians(45.807434)) *
// 				cos(radians(latitude)) *
// 				cos(radians(16.02378674499556 - longitude))
// 			) * 6371
// 		) as distance from locations
// 	)
// locations where distance < 10 where order by distance;
func (btm *BikeTheftManager) Nearest(ctx context.Context, f models.NearestBikeTheftsFilter, limit int32) ([]models.BikeTheft, error) {
	q := `select
			bike_thefts.id,
			bike_thefts.user_id,
			bike_thefts.street,
			bike_thefts.city,
			bike_thefts.description,
			bike_thefts.latitude,
			bike_thefts.longitude,
			bike_thefts.created_at,
			bike_thefts.updated_at,
			users.id,
			users.username,
			users.full_name,
			users.is_active,
			users.created_at,
			users.updated_at
			FROM 
			(
				SELECT *,
				(
					ACOS(
						SIN(RADIANS($1)) *
						SIN(RADIANS(latitude)) +
						COS(radians($2)) *
						COS(RADIANS(latitude)) *
						COS(RADIANS($3 - longitude))
					) * 6371
				) AS distance FROM bike_thefts WHERE deleted_at IS NULL
			) bike_thefts
			JOIN users ON users.id = bike_thefts.user_id
			WHERE distance < $4 order by distance limit $5;`

	rows, err := btm.db.db.Query(ctx, q, f.Latitude, f.Latitude, f.Longitude, f.Distance, limit)
	if err != nil {
		return nil, NewError(err, "get nearest bike thefts")
	}
	defer rows.Close()

	var thefts []models.BikeTheft
	for rows.Next() {
		var bt models.BikeTheft
		var u models.User
		if err := rows.Scan(
			&bt.ID,
			&bt.UserID,
			&bt.Street,
			&bt.City,
			&bt.Description,
			&bt.Latitude,
			&bt.Longitude,
			&bt.CreatedAt,
			&bt.UpdatedAt,
			&u.ID,
			&u.Username,
			&u.FullName,
			&u.IsActive,
			&u.CreatedAt,
			&u.UpdatedAt,
		); err != nil {
			return nil, err
		}
		bt.User = u
		thefts = append(thefts, bt)
	}

	return thefts, nil
}
