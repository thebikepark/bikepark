package bikethefts

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
)

type Writer interface {
	Create(ctx context.Context, bp models.BikeTheft) (models.BikeTheft, error)
	Update(ctx context.Context, bp models.BikeTheft) (models.BikeTheft, error)
	Delete(ctx context.Context, ID, userID uuid.UUID) error
}

func Create(ctx context.Context, bt models.BikeTheft, w Writer) (models.BikeTheft, error) {
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return bt, bpErrors.NewInteralServerError("failed to get requester from ctx", err)
	}

	bt.UserID = r.UserID

	return w.Create(ctx, bt)
}

func Update(ctx context.Context, bt models.BikeTheft, w Writer) (models.BikeTheft, error) {
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return bt, bpErrors.NewInteralServerError("failed to get requester from ctx", err)
	}

	bt.UserID = r.UserID

	return w.Update(ctx, bt)
}

func Delete(ctx context.Context, UUID uuid.UUID, w Writer) error {
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return bpErrors.NewInteralServerError("failed to get requester from ctx", err)
	}

	return w.Delete(ctx, UUID, r.UserID)
}
