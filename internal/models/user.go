package models

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type (
	// User represents database user
	User struct {
		ID        uuid.UUID
		Password  string
		Username  string
		FullName  sql.NullString
		IsActive  bool
		CreatedAt time.Time
		UpdatedAt time.Time
		ExpiresAt sql.NullTime
		DeletedAt sql.NullTime

		UserLocation UserLocation
	}

	UserLocation struct {
		Address   sql.NullString
		Latitude  sql.NullFloat64
		Longitude sql.NullFloat64
	}

	// AuthUser holds data required only for auth
	AuthUser struct {
		ID       uuid.UUID
		Username string
		Password string
	}

	UsersFilter struct {
	}
)

// APIUserToData translates API user into data user object
func APIUserToData(u *v1.User) User {
	return User{
		FullName: sql.NullString{
			String: u.GetFullName(),
			Valid:  u.GetFullName() != "",
		},
		Password:     u.GetPassword(),
		Username:     u.GetUsername(),
		IsActive:     u.GetIsActive(),
		UserLocation: APIUserLocationToData(u.GetUserLocation()),
	}
}

func APIUserLocationToData(ul *v1.UserLocation) UserLocation {
	if ul == nil {
		return UserLocation{}
	}

	return UserLocation{
		Address: sql.NullString{
			String: ul.Address,
			Valid:  ul.GetAddress() != "",
		},
		Latitude: sql.NullFloat64{
			Float64: ul.GetLatitude(),
			Valid:   ul.GetLatitude() > 0,
		},
		Longitude: sql.NullFloat64{
			Float64: ul.GetLongitude(),
			Valid:   ul.GetLongitude() > 0,
		},
	}
}

func DataUserLocationToAPI(ul UserLocation) *v1.UserLocation {
	return &v1.UserLocation{
		Address:   ul.Address.String,
		Latitude:  ul.Latitude.Float64,
		Longitude: ul.Longitude.Float64,
	}
}

// DataUserToAPI translates data user into API user
func DataUserToAPI(u User) *v1.User {
	return &v1.User{
		Name:         app.UserResourceName(u.ID),
		FullName:     u.FullName.String,
		UserLocation: DataUserLocationToAPI(u.UserLocation),
		Username:     u.Username,
		IsActive:     u.IsActive,
		CreatedAt:    timestamppb.New(u.CreatedAt),
		UpdatedAt:    timestamppb.New(u.UpdatedAt),
	}
}

// DataUsersToAPI translets list of data user into API list of users
func DataUsersToAPI(us []User) []*v1.User {
	apiUsers := make([]*v1.User, len(us))
	for i, u := range us {
		apiUsers[i] = DataUserToAPI(u)
	}
	return apiUsers
}

// AuthUserToAPI translates authenticated user to API user
func AuthUserToAPI(au *AuthUser) *v1.User {
	if au == nil {
		return &v1.User{}
	}

	return &v1.User{
		Name:     app.UserResourceName(au.ID),
		Username: au.Username,
	}
}
