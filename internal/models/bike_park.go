package models

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type (
	// Location represents database user
	BikePark struct {
		ID           uuid.UUID
		UserID       uuid.UUID
		Street       string
		City         string
		IsSuggestion bool
		Description  string
		Latitude     float64
		Longitude    float64
		CreatedAt    time.Time
		UpdatedAt    time.Time

		User User
	}

	// NearestLocationFilter holds info to filter and limit locations
	NearestLocationFilter struct {
		Distance            int32
		Latitude, Longitude float64
	}

	// BikeparkFilter holds data for locations filtering
	BikeparkFilter struct {
		UserID       uuid.UUID
		Street       string
		City         string
		IsSuggestion bool
		IsDeleted    *bool
	}
)

// APIParkingToData translates API bike parking into data biketheft
func APIParkingToData(bp *v1.CreateEditBikeParking) BikePark {
	return BikePark{
		City:         bp.GetCity(),
		IsSuggestion: bp.IsSuggestion,
		Street:       bp.GetStreet(),
		Description:  bp.GetDescription(),
		Latitude:     bp.GetLatitude(),
		Longitude:    bp.GetLongitude(),
		// CreatedAt:    time.Unix(bp.GetCreatedAt().GetSeconds(), int64(bp.GetCreatedAt().GetNanos())),
		// UpdatedAt:    time.Unix(bp.GetUpdatedAt().GetSeconds(), int64(bp.GetUpdatedAt().GetNanos())),
	}
}

// DataParkingToAPI translates data parking to API parking
func DataParkingToAPI(bt BikePark) (*v1.BikeParking, error) {
	return &v1.BikeParking{
		Name:         app.BikeParkResourceName(bt.ID),
		IsSuggestion: bt.IsSuggestion,
		City:         bt.City,
		Street:       bt.Street,
		Description:  bt.Description,
		User:         DataUserToAPI(bt.User),
		Latitude:     bt.Latitude,
		Longitude:    bt.Longitude,
		CreatedAt:    timestamppb.New(bt.CreatedAt),
		UpdatedAt:    timestamppb.New(bt.UpdatedAt),
	}, nil
}

// DataParkingsToAPI translates list of data bike parkings to API bike parkings
func DataParkingsToAPI(bts []BikePark) ([]*v1.BikeParking, error) {
	apiParkings := make([]*v1.BikeParking, len(bts))

	for i, bt := range bts {
		apiTheft, err := DataParkingToAPI(bt)
		if err != nil {
			return nil, err
		}
		apiParkings[i] = apiTheft
	}

	return apiParkings, nil
}
