package models

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type (
	// BikeTheft database bike theft data
	BikeTheft struct {
		ID          uuid.UUID
		UserID      uuid.UUID
		Street      string
		City        string
		Latitude    float64
		Longitude   float64
		Description string
		IsFound     bool
		CreatedAt   time.Time
		UpdatedAt   time.Time

		User User
	}

	// BikeTheftsFilter database query conditions
	BikeTheftsFilter struct {
		UserID    uuid.UUID
		IsDeleted *bool
	}

	// NearestBikeTheftsFilter holds nearest bike theft filter params
	NearestBikeTheftsFilter struct {
		Distance            int32
		Latitude, Longitude float64
	}
)

// APITheftToData translates API bike theft into data biketheft
func APITheftToData(bt *v1.BikeTheft) BikeTheft {
	if bt == nil {
		return BikeTheft{}
	}

	id, err := app.ExtractIDFromResourceName(bt.GetName(), 2)
	if err != nil {
		id = uuid.Nil
	}

	return BikeTheft{
		ID:          id,
		City:        bt.GetCity(),
		Street:      bt.GetStreet(),
		Description: bt.GetDescription(),
		Latitude:    bt.GetLatitude(),
		Longitude:   bt.GetLongitude(),
		IsFound:     bt.GetIsFound(),
		CreatedAt:   time.Unix(bt.GetCreatedAt().GetSeconds(), int64(bt.GetCreatedAt().GetNanos())),
		UpdatedAt:   time.Unix(bt.GetUpdatedAt().GetSeconds(), int64(bt.GetUpdatedAt().GetNanos())),
	}
}

// DataTheftsToAPI translates list of data bike thefts to API bike thefts
func DataTheftsToAPI(bts []BikeTheft) []*v1.BikeTheft {
	apiThefts := make([]*v1.BikeTheft, len(bts))

	for i, bt := range bts {
		apiThefts[i] = DataTheftToAPI(bt)
	}

	return apiThefts
}

// DataTheftToAPI translates data theft to API theft
func DataTheftToAPI(bt BikeTheft) *v1.BikeTheft {
	return &v1.BikeTheft{
		Name:        app.BikeTheftResourceName(bt.ID),
		City:        bt.City,
		Street:      bt.Street,
		Description: bt.Description,
		User:        DataUserToAPI(bt.User),
		Latitude:    bt.Latitude,
		Longitude:   bt.Longitude,
		IsFound:     bt.IsFound,
		CreatedAt:   timestamppb.New(bt.CreatedAt),
		UpdatedAt:   timestamppb.New(bt.UpdatedAt),
	}
}
