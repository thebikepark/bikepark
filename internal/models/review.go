package models

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type (
	// Review holds review data
	Review struct {
		ID        uuid.UUID
		UserID    uuid.UUID
		PlaceID   uuid.UUID
		User      User
		Comment   string
		Rating    int32
		CreatedAt time.Time
		UpdatedAt time.Time
	}

	ReviewsFilter struct {
		PlaceID uuid.UUID
		UserID  uuid.UUID
	}
)

func ReviewToAPIReview(r Review) (*v1.Review, error) {
	return &v1.Review{
		Name:    app.ReviewResourceName(r.ID),
		UserId:  r.UserID.String(),
		PlaceId: r.PlaceID.String(),
		User: &v1.User{
			Username: r.User.Username,
		},
		Comment:   r.Comment,
		Rating:    r.Rating,
		CreatedAt: timestamppb.New(r.CreatedAt),
		UpdatedAt: timestamppb.New(r.UpdatedAt),
	}, nil
}

func APIReviewToData(r *v1.Review) (Review, error) {
	// we do not have resource, try to create a new one
	reviewID, _ := app.ExtractIDFromResourceName(r.Name, 2)

	var placeUUID uuid.UUID
	if r.GetPlaceId() != "" {
		var err error
		placeUUID, err = uuid.Parse(r.GetPlaceId())
		if err != nil {
			return Review{}, err
		}
	}

	var userUUID uuid.UUID
	if r.GetUserId() != "" {
		var err error
		userUUID, err = uuid.Parse(r.GetUserId())
		if err != nil {
			return Review{}, err
		}
	}

	return Review{
		ID:        reviewID,
		PlaceID:   placeUUID,
		UserID:    userUUID,
		Comment:   r.GetComment(),
		Rating:    r.GetRating(),
		CreatedAt: r.GetCreatedAt().AsTime(),
		UpdatedAt: r.GetCreatedAt().AsTime(),
	}, nil
}
