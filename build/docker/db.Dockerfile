FROM postgres:14.1

# install the migrate tool
RUN apt-get update && apt-get install -y curl apt-transport-https
RUN curl -L https://packagecloud.io/golang-migrate/migrate/gpgkey | apt-key add -
RUN echo "deb https://packagecloud.io/golang-migrate/migrate/ubuntu/ xenial main" > /etc/apt/sources.list.d/migrate.list
RUN apt-get update && \
    apt-get install -y migrate

ARG MIGRATIONS_PATH
ARG POSTGRES_DB
ARG POSTGRES_USER
ARG POSTGRES_PASSWORD
ARG POSTGRES_HOST
ARG POSTGRES_PORT

# default postgres config
ENV MIGRATIONS_PATH=$MIGRATIONS_PATH \
    POSTGRES_DB=$POSTGRES_DB \
    POSTGRES_USER=$POSTGRES_USER \
    POSTGRES_PASSWORD=$POSTGRES_USER

# copy over migrations
COPY assets/migrations /migrations

# copy migration script to the init dir
# to be executed on the first run of a container
COPY build/scripts/migrate.sh /docker-entrypoint-initdb.d/migrate.sh

EXPOSE 5432 54320
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD [ "postgres" ]