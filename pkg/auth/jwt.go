package auth

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

// BikeParkClaims claims data
type (
	BikeParkClaims struct {
		*jwt.StandardClaims
		UserClaims
	}

	UserClaims struct {
		Username string    `json:"username"`
		UserID   uuid.UUID `json:"user_id"`
	}
)

// NewBikeClaims retruns new bikepark claims with username
func (a *Auth) NewBikeClaims(u UserClaims) BikeParkClaims {
	return BikeParkClaims{
		UserClaims: u,
		StandardClaims: &jwt.StandardClaims{
			Subject:   a.Subject,
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(a.TokenDuration).Unix(),
		},
	}
}

// GenerateJWTToken generates jwt token from claims
func GenerateJWTToken(c jwt.Claims, key *rsa.PrivateKey) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, c)
	return token.SignedString(key)
}

// ValidateAndReturnCalimsFromToken validate and returns claims from token
func ValidateAndReturnCalimsFromToken(t string, key *rsa.PublicKey) (*BikeParkClaims, error) {
	var claims BikeParkClaims
	token, err := jwt.ParseWithClaims(t, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method")
		}
		return key, nil
	})
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	if err := claims.Valid(); err != nil {
		return nil, fmt.Errorf("invalid claims: %w", err)
	}

	return &claims, nil
}

// ReadPrivateKey read private key
func ReadPrivateKey(path string) (*rsa.PrivateKey, error) {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	key, err := jwt.ParseRSAPrivateKeyFromPEM(buf)
	if err != nil {
		return nil, err
	}
	return key, nil
}
