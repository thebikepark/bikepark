package auth

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	authHeader = "grpcgateway-authorization"
	// UsernameHeaderKey username header key
	UsernameHeaderKey = "username"
	// NameHeaderKey username header key
	UUIDHeaderKey = "user_id"
	// ProtoService proto service name
	ProtoService = "bikepark.BikePark"
	// ProtoAuthService proto auth service name
	ProtoAuthService = "bikepark.BikeParkAuth"
)

var (
	publicMethods = map[string]struct{}{
		"/" + ProtoService + "/" + "ListBikeParkings":    {},
		"/" + ProtoService + "/" + "ListBikeThefts":      {},
		"/" + ProtoService + "/" + "NearestBikeParkings": {},
		"/" + ProtoService + "/" + "NearestBikeThefts":   {},
		"/" + ProtoService + "/" + "ListReviews":         {},
		"/" + ProtoAuthService + "/" + "Login":           {},
		"/" + ProtoAuthService + "/" + "Register":        {},
	}
)

var (
	errInvalidAuthHeader = status.New(
		codes.Internal,
		"Problem parsing authorization header",
	)
	errInvalidHeader       = errors.New("invalid authorization header")
	errInvalidJwtToken     = errors.New("invalid jwt token")
	errNoUserIDInJwtClaims = errors.New("no user id in jwt claims")
)

// Interceptor authentication interceptor
func Interceptor(key *rsa.PublicKey) grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		if IsPublic(method) {
			return invoker(ctx, method, req, reply, cc, opts...)
		}

		ctx, err := validate(ctx, key)
		if err != nil {
			switch err {
			case errInvalidHeader:
				return errInvalidAuthHeader.Err()
			case errInvalidJwtToken, errNoUserIDInJwtClaims:
				return status.Error(
					codes.Unauthenticated,
					"Your authentication token is not valid, try to log in again and please contact support if problem persists.",
				)
			default:
				return status.Error(codes.Internal, "A problem occured. Please contanct support.")
			}
		}
		return invoker(ctx, method, req, reply, cc, opts...)
	}
}

// IsPublic checks is method public
func IsPublic(m string) bool {
	_, ok := publicMethods[m]
	return ok
}

func validate(ctx context.Context, key *rsa.PublicKey) (context.Context, error) {
	md, ok := metadata.FromOutgoingContext(ctx)
	if !ok {
		return ctx, fmt.Errorf("get metadata from context: %w", errInvalidHeader)
	}

	authHeader := md.Get(authHeader)
	if authHeader == nil || len(authHeader) != 1 {
		return ctx, fmt.Errorf("get authorization header: %w", errInvalidHeader)
	}

	authVals := strings.Split(authHeader[0], " ")
	if len(authVals) != 2 || authVals[0] != "Bearer" {
		return ctx, fmt.Errorf("parse authorization header into values: %w", errInvalidHeader)
	}

	claims, err := ValidateAndReturnCalimsFromToken(authVals[1], key)
	if err != nil {
		return ctx, fmt.Errorf("validate and return calims from token: %w", errInvalidJwtToken)
	}

	if claims.UserID == uuid.Nil {
		return ctx, errNoUserIDInJwtClaims
	}

	kv := []string{
		UsernameHeaderKey, claims.Username,
		UUIDHeaderKey, claims.UserID.String(),
	}
	ctx = metadata.AppendToOutgoingContext(ctx, kv...)
	return ctx, nil
}
